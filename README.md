# invex

Invidious URL grabber

invex is a simple CLI tool which lets you search for YouTube videos through 
Invidious. The result gives you a URL which you can use for anything -- you 
can paste the URL to mpv, youtube-dl, or anywhere else you'd like. Gone are 
the days where you have to open up a browser and jump through menus and 
loading times and thumbnails of silly faces, ads, and clickbait.

Inspired by [mpv](https://mpv.io) and [youtube-dl](https://youtube-dl.org/),
invex was created so that both tools could be used without the need to open a
browser to get the URL in the first place.

You can see all the screenshots here: 
[SCREENSHOTS](https://gitlab.com/steelinferno/invex/tree/screenshots)

### Usage

1. Run this in the terminal:

        git clone https://gitlab.com/steelinferno/invex.git && cd invex
        ./invex.py

   ![startup](https://gitlab.com/steelinferno/invex/raw/screenshots/step1.png)

2. Type in what you want to search, After the results load, it'll prompt you to
   pick a number. 

   ![results](https://gitlab.com/steelinferno/invex/raw/screenshots/step2.png)

3. For the final prompt, here are some sample options:

        >0 # copies the URL of the first video to the clipboard
        >mpv 0 # streams the first video in mpv 
        >vlc 0 # opens the first video in vlc
        >ydl 0 # downloads first the video
        >youtube-dl 0 # downloads the first video

   ![end](https://gitlab.com/steelinferno/invex/raw/screenshots/step3.png)

You are now ready to easily search and view videos from the command line. Enjoy!

### Known issues

- st (simple terminal) crashes if you search for "best pirate car". tty works 
fine. Needs more testing. 
- fish terminal needs quotes around the URL to open with mpv and such

**[ytdl-hook] youtube-dl failed: unexpected error ocurred**

Try updating youtube-dl. It needs to be regularly updated to work, since it's 
a web scraper and not an api.

**ffmpeg - tcp: Failed to resolve hostname**

Try updating ffmpeg. At this point you should probably update your whole system.

### New in v0.6

- Views and video length now shown
- Emojis
- URL removed; no longer needed since copy feature exists
- Gave the screenshots their own branch
- numerous optimizations:

        pylint invex.py 
        ************* Module invex
        invex.py:8:4: W0622: Redefining built-in 'exit' (redefined-builtin)
        
        ------------------------------------------------------------------
        Your code has been rated at 9.87/10 (previous run: 3.45/10, +6.42)

### Further plans

- animated loading ascii for the mpv option
- add a config file
- url-sanitizing tools
- add a man page
- add invex to repos
- exception handling for having both 'mpv' and 'youtube-dl' in the final prompt

Do you like invex? Feel free to star my project and move the script to /bin:

    sudo mv invex.py invex && sudo chmod +x invex && sudo mv invex /bin/

invex is much faster than mps-youtube and it uses Invidious API instead of
YouTube API!
