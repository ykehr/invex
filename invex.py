#!/usr/bin/python3
'''
invex v0.6 by steelinferno
special thanks for feedback from friends
and an online community of technologically advanced anonymous people
'''
try:
    from sys import argv, exit
    from os import environ
    from subprocess import run
    from shutil import which
    from locale import setlocale, LC_ALL
    from pyperclip import copy
    from json import decoder
    from requests import get, exceptions
    from colorama import Fore, Style
    from datetime import timedelta
except ImportError: # most common issue reported from testers
    print('Missing libraries! Run this command:\n'
          + '"pip3 install requests pyperclip colorama --user"')
    exit()

# Style settings
setlocale(LC_ALL, '')
COLOR_A = Fore.GREEN # See https://github.com/tartley/colorama
COLOR_B = Fore.RED # for a list of available colors
VIEWS = '👀️: '
LENGTH = '🕐️: '
PROMPT = '>'
URL = 'https://invidio.us/'

print(COLOR_A + 'Invex - Invidio.us URL grabber')

# if arguments are given, use that. If not, ask for input.
if len(argv) > 1:
    ARGS = ' '.join(argv[1:])
    ISQ = ARGS.replace(' ', '+') # ISQ = input search query
else:
    print(Style.NORMAL + '\nInput search query:')
    ISQ = input(PROMPT).replace(' ', '+')

# print search query URL
SQ = URL + 'search?q=' + ISQ # SQ = search query
print(Style.DIM + SQ + Style.RESET_ALL)

# get API
API = URL + 'api/v1/search?q=' + ISQ
try:
    j = get(API).json()
except exceptions.ConnectionError: # no internet
    print(COLOR_B + Style.BRIGHT
          + 'cannot connect to "' + URL + '", please try again later.')
    exit()
except decoder.JSONDecodeError: # invidio.us is down
    print(COLOR_B + Style.BRIGHT + 'Service is currently unavailable, please try again later.')
    exit()

# declare variables for the search result loop
NUM = [] # reference number to the URL on the printed list
TITLE = [] # title of the reference
k = -1 # reference number
PREFIX = ' ' # to align results evenly

# loop through results
for i in j:
    if k < 9:
        PREFIX = ' '
    else:
        PREFIX = ''
    k += 1
    print(Style.BRIGHT + COLOR_A + PREFIX + str(k) + Style.DIM + '| '
          + Style.RESET_ALL + Style.BRIGHT + COLOR_B
          + '%s ' % (i['title']) + Style.RESET_ALL
          + VIEWS + Style.BRIGHT + '%s ' % format(i['viewCount'], 'n') + Style.RESET_ALL
          + LENGTH + Style.BRIGHT + '%s' % (str(timedelta(seconds=i['lengthSeconds'])))
          + Style.RESET_ALL)
    # index the URLs and titles to a number that can be called on later
    NUM.append(str(URL + 'watch?v=%s' % (i['videoId'])))
    TITLE.append(str(i['title']))

# final prompt
APPS = 'mpv', 'youtube-dl', 'vlc'
def openapp(app):
    '''
    *input example*: 'mpv 0'
    This method prints a loading message and runs a program from the above list
    '''
    print('loading "' + (TITLE[int(PICK.replace(app, ''))]) + '"')
    run([which(app), NUM[int(PICK.replace(app, ''))]], check=True)
    exit()
while True:
    print(COLOR_A + 'Pick a number to copy URL, or run number with mpv or youtube-dl:')
    PICK = input(PROMPT) # ordinary input for clipboard usage
    PICK_COMM = PICK.lower().split(' ') # split input for commands
    try:
        if environ['XDG_SESSION_TYPE'] == 'wayland':
            try:
                run([which('wl-copy'), NUM[int(PICK)]], check=True)
                print('copied "'+ (TITLE[int(PICK)]) +'" to the clipboard!')
                exit()
            except FileNotFoundError:
                print('Install "wl-clipboard" to use this feature. Run:\n'
                      +'sudo apt install wl-clipboard')
                exit()
        else:
            copy(NUM[int(PICK)])
            print('copied "'+ (TITLE[int(PICK)]) +'" to the clipboard!')
            exit()
    except (ValueError, IndexError) as exception:
        if PICK_COMM[0] in APPS:
            openapp(PICK_COMM[0])
        else:
            print(COLOR_B + 'Invalid command -- try "0", "mpv 0", "vlc 0", or'
                  + ' "youtube-dl 0".\nAlso, verify that the number is between 0-19.' + COLOR_A)
